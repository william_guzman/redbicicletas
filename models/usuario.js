const mongoose = require('mongoose')
const uniqueVaalidator = require('mongoose-unique-validator')
const Schema = mongoose.Schema
const Reserva = require('./reserva')
const Token = require('./token')
const bcrypt = require('bcrypt')
const crypto = require('crypto')
const sendMail = require('../providers/mail')
const mailer = require('../mailer/mailer')
const saltRounds = 10

const validateEmail = function(email){
    const regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
    return regex.test(email)
}

const usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true, 
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        validate: [ validateEmail, 'Por favor, ingrese un email válido' ],
        unique: true,
        match: [/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i]
    },
    password: {
        type: String,
        required: [ true, 'El password es obligatorio'],
    },
    passwordResetToken: String,
    passwordRestTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: {
        type: String,
    },
    facebookId: {
        type: String,
    },
})

usuarioSchema.plugin(uniqueVaalidator, { message: 'El {PATH} ya existe con otro usuario' });

usuarioSchema.pre('save', function(next){
    if ( this.isModified('password') ){
        this.password = bcrypt.hashSync(this.password, saltRounds)
    }
    next()
})

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password)
}

usuarioSchema.methods.enviar_email_bienvenida = async function(){
    const token = new Token({ usuario:this.id, token: crypto.randomBytes(16).toString('hex') })
    // const email_destination = this.email
    try {
        await token.save()
        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: this.email,
            subject: 'Verificación de cuenta',
            text: 'Hola, \n\n Por favor, para verificar su cuenta haga click en este enlace: \n http://localhost:3000'+'\/token/confirmation\/'+token.token+'\n'
        }

        const emailResponse = await sendMail(mailOptions)
        console.log('Se ha enviado un email a '+this.email)

    } catch (error) {
        console.log(error)
    }
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, callback){
    const reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde, hasta})
    console.log('reservar@reserva', reserva)
    reserva.save(callback)
}

usuarioSchema.methods.resetPassword = function(callback){
    const token = new Token({ usuario: this._id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;
    token.save(function (err){
        if(err) return callback(err)

        const mailOptions = {
            from: process.env.EMAIL_FROM,
            to:email_destination,
            subject: 'Reseteo de password de cuenta',
            text: `Hola para resetear tu cuenta dirijase a este enlace: 
                ${process.env.HOST}/resetPassword/token.token.`
        }

        mailer.sendMail(mailOptions, function(err){
            if (err) return callback(err)

            console.log(`Se envio un email para resetear el password a: ${email_destination}`)

        })
        callback(null)
    })
}

usuarioSchema.statics.removeById = function (_id) {
    return this.deleteOne({ _id });
};

usuarioSchema.statics.findOneOrCreatebyFacebook = function(profile, callback){

    const self = this;
    console.log(profile);
    self.findOne(
        {
            $or: [
                { facebookId: profile.id },
                { email: profile.emails[0].value },
            ],
        },
        (err, result) => {
            if (result) {
                callback(err, result);
            } else {
                console.log("--------CONDITION--------");
                console.log(profile);
                let values = {};
                values.facebookId = profile.id;
                values.email = profile.emails[0].value || 'luijo@correo.local';
                values.nombre = profile.displayName || "SIN NOMBRE";
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                console.log("--------VALUES--------");
                console.log(values);
                self.create(values, (err, result) => {
                    if (err) {
                        console.log(err);
                    }
                    return callback(err, result);
                });
            }
        }
    );


}

usuarioSchema.statics.findOneOrCreateByGoogle = function(profile, callback){
    const self = this;
    console.log(profile);
    self.findOne({
        $or: [
            {'googleId': profile.id},
            {'email': profile.emails[0].value}
        ]
    }, (err, result) => {
        if (result) {
            callback(err, result)
        } else {

            console.log('--------CONDITION--------')
            console.log(profile)
            let values = {};
            values.googleId = profile.id;
            values.email = profile.emails[0].value;
            values.nombre = profile.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = profile._json.etag;
            console.log("--------VALUES--------");
            console.log(value);
            self.create(values, (err, result) => {
                if (err) { console.log(err); }
                return callback(err, result)
            })
        }
    })
}

module.exports = mongoose.model('Usuario', usuarioSchema)