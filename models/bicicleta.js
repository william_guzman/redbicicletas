const mongoose = require('mongoose')
const Schema = mongoose.Schema

const bicicletaSchema = new Schema({
    code: Number, 
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number],
        index: {
            type: '2dsphere',
            sparse: true
        }
    }
})

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion){
    return new this({
        code,
        color,
        modelo,
        ubicacion,
    })
}


bicicletaSchema.methods.toString = function (){
    return `code: ${this.code} | color ${this.color}` 
}

bicicletaSchema.statics.allBicis = function (callback = null){
    if (callback == null) {
        return this.find({})
    }
    return this.find({}, callback)
}

bicicletaSchema.statics.add = function(newBici, callback = null){
    if (callback == null) {
        return this.create(newBici)
    }
    return this.create(newBici, callback)
}

bicicletaSchema.statics.findByCode = function(code, callback){
    if (callback == null) {
        return this.findOne({ code })
    }
    return this.findOne({code}, callback)
}
bicicletaSchema.statics.removeByCode = function(code, callback){
    return this.deleteOne({code}, callback)
}


module.exports = mongoose.model('Bicicleta', bicicletaSchema)
/*
const Bicicleta = function (id, color, modelo, ubicacion){

    this.id = id
    this.color = color
    this.modelo = modelo
    this.ubicacion = ubicacion

}

Bicicleta.prototype.toString = function() {
    return `id: ${this.id} | color: ${this.color}`
}

Bicicleta.allBicis = []
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici)
}

Bicicleta.findById = function(aBiciId) {
    const aBici = Bicicleta.allBicis.find(x => x.id == aBiciId)

    if (aBici) return aBici

    throw new Error(`No existe una bicicleta con el id ${aBiciId}`)
}

Bicicleta.removeById = function (aBiciId){
    const aBiciIndex = Bicicleta.allBicis.findIndex(bici => bici.id == aBiciId)
    if (aBiciIndex >= 0) {
        Bicicleta.allBicis.splice(aBiciIndex, 1)
    }
}

// let a = new Bicicleta(1, 'rojo', 'urbana', [10.4664855, -66.5364315])
// let b = new Bicicleta(2, 'verde', 'urbana', [10.4589894, -66.5325691])

// Bicicleta.add(a)
// Bicicleta.add(b)

module.exports = Bicicleta
*/