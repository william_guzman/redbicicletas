const express = require('express')
const router = express.Router()
const bicicletaController = require('../controllers/bicicleta')

router.get('/', bicicletaController.bicicletaList)
router.get('/create', bicicletaController.bicicletaCreateView)
router.post('/create', bicicletaController.bicicletaCreate)
router.post('/:id/delete', bicicletaController.bicicletaRemove)
router.get('/:id/update', bicicletaController.bicicletaUpdateView)
router.post('/:id/update', bicicletaController.bicicletaUpdate)

module.exports = router