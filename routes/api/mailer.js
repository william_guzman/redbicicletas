const express = require("express");
const router = express.Router();
const mailer = require("../../mailer/mailer")

router.get("/:email", async function (req, res) {
    const mailOptions = {
        from: process.env.EMAIL_FROM, // sender address
        to: req.params.email, // list of receivers
        subject: "Hello ✔", // Subject line
        text: "Hola desde mi Frankenstein", // plain text body
        html: "<b>Hola desde mi Frankenstein</b>", // html body
    };
    try {
        await mailer.sendMail(mailOptions);
        res.json({
            success: true
        })
    } catch (error) {
        console.log(error)
        res.status(400).json({
            success: false,
            error: error.message
        })
    }
});

module.exports = router;