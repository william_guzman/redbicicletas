const Bicicleta = require('../../models/bicicleta')

const bicicletaList = async (req, res) => {
    res.status(200).json({
        bicicletas: await Bicicleta.find({})
    })
}

const bicicletaCreate = function(req, res){
    const bici = fnBicicletaCreate(req.body);
    res.status(201).json({
      bici,
    });
}

const bicicletaUpdate = async (req, res) => {
  const aBici = await Bicicleta.findByCode(req.params.id);
  const { color, modelo, lat, lng } = req.body;

  aBici.color = color;
  aBici.modelo = modelo;
  aBici.ubicacion = [lat, lng];
  aBici.save()

  res.status(200).json(aBici)
};

const bicicletaRemove = async (req, res) => {
  const bici = await Bicicleta.removeByCode(req.params.id);
  res.status(200).json({ bicicleta: bici });
};

function fnBicicletaCreate(aBici){

    console.log("fnBicicletaCreate@aBici", aBici);
    const { code, color, modelo, lat, lng } = aBici;

    const bici = new Bicicleta({code, color, modelo});
    bici.ubicacion = [lat, lng];
    bici.save()
    return bici
}

module.exports = {
  fnBicicletaCreate,
  bicicletaList,
  bicicletaCreate,
  bicicletaUpdate,
  bicicletaRemove,
};