const Usuario = require("../models/usuario");

const list = async function(req, res, next){
    const usuarios = await Usuario.findById(req.params.id)
    res.render('usuarios/index', { usuarios })
}

const update_get = async function(req, res, next){
    let usuarios = null
    try {
        usuarios = await Usuario.findById(req.params.id);
    } catch (error) {
        
    }
    res.render('usuarios/update', { errors:{}, usuarios })
}

const update = async function (req, res, next) {
    let updated_values = { nombre:req.bode.nombre }
    try {
        await Usuario.findByIdAndUpdate(req.params.id, updated_values)
        res.redirect('/usuarios')
    } catch (error) {
        console.log(error)
        const usuario = new Usuario({ email: req.body.email, nombre: req.body.nombre })
        res.render('usuarios/update', { errors: error.errors, usuario })
    }
};

const create_get = async function(req,res,next){
    res.render("usuarios/create", {
        errors: {},
        usuario: new Usuario({
            email: req.body.email,
            nombre: req.body.nombre,
        }),
    });
}

const create = async function (req, res, next){

    if (req.body.password != req.body.confirm_password) {
        res.render("usuarios/create", {
            errors: {
                confirm_password: {
                    message: "No coincide con el password ingresado",
                },
            },
            usuario: new Usuario({
                email: req.body.email,
                nombre: req.body.nombre,
            }),
        });
        return
    }

    try {
        const { nombre, email, password } = req.body
        const nuevoUsuario = await Usuario.create({ nombre, email, password })
        nuevoUsuario.enviar_email_bienvenida()
        res.redirect('/usuarios')
    } catch (error) {
        const usuario = new Usuario({
            email: req.body.email,
            nombre: req.body.nombre,
        });
        res.render("usuarios/create", { errors: error.errors, usuario });
    }

}

const deleteFn = async function (req, res, next){
    try {
        await Usuario.findByIdAndDelete(req.body.id)
        res.redirect('/usuarios')
    } catch (error) {
        next(error)
    }
}

module.exports = {
    list,
    update_get,
    update,
    create_get,
    create,
    deleteFn,
}