const passport = require('passport')

const LocalStrategy = require('passport-local').Strategy
const usuario = require('../models/usuario')
const GoogleStrategy = require('passport-google-oauth20').Strategy
const FacebookTokenStrategy = require('passport-facebook-token')

passport.use(
    new FacebookTokenStrategy(
        {
            clientID: process.env.FACEBOOK_APP_ID,
            clientSecret: process.env.FACEBOOK_APP_SECRET,
            fbGraphVersion: "v3.0",
        },
        function (accessToken, refreshToken, profile, done) {

            try {
                usuario.findOneOrCreatebyFacebook(profile, function(err, user){
                    if(err) console.log('err', err);
                    return done(err, user)
                })
            } catch (err) {
                console.log(err)
                return done(err, null)
            }

        }
    )
);

passport.use(new LocalStrategy(
    function(email, password, done){
        usuario.findOne({email}, function(err, usuario){
            if(err) return done(err);
            let message = 'Email no existente o incorrecto';
            if (!usuario) return done(null, false, {message});
            message = 'Password incorrecto';
            if (!usuario.validPassword(password)) return done(null, false, { message });

            return done(null, usuario);
        });
    }
));

passport.use(
    new GoogleStrategy(
        {
            clientID: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_SECRET,
            callbackURL: process.env.HOST + "/auth/google/callback",
        },
        function (accessToken, refresToken, profile, cb) {
            console.log(profile);

            usuario.findOneOrCreateByGoogle(profile, function (err, user) {
                return cb(err, user);
            });
        }
    )
);

passport.serializeUser(function(user, callback){
    callback(null, user.id)
})

passport.deserializeUser(function(id, callback){
    usuario.findById(id, function(err, usuario){
        callback(err, usuario)
    })
})

module.exports = passport
